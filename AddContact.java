import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.border.Border;
import javax.swing.table.AbstractTableModel;
class AddContact extends JFrame implements ActionListener
{
	MainFrame mf;
	Validator va;
	JLabel name,gender,dob,email,phone,address;
	JLabel nameErrorMessage,genderErrorMessage,dobErrorMessage,emailErrorMessage,phoneErrorMessage,addressErrorMessage;
	JTextField namefield,emailfield,phonefield,dobfield;
	JTextArea addressfield;
	JButton submit,back;
	JRadioButton male,female,other;
	ButtonGroup bg;
	Container contentPane;
	String genderfield="";
	StringBuffer xmlCode;

	Border redline,grayline;
	AddContact(MainFrame mf)
	{
		super("AddContact");
		setSize(600,600);
		va=new Validator();
		this.mf=mf;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		redline = BorderFactory.createLineBorder(Color.red);
		grayline = BorderFactory.createLineBorder(Color.gray);
		setVisible(true);
	}

	public void displayForm()
	{
		contentPane=getContentPane();
		contentPane.setLayout(null);

		name=new JLabel("Name : ");
		gender=new JLabel("Gender : ");
		dob=new JLabel("Date of birth : ");
		email=new JLabel("Email : ");
		phone=new JLabel("Phone no : ");
		address=new JLabel("Address : ");

		nameErrorMessage=new JLabel("");
		genderErrorMessage=new JLabel("");
		emailErrorMessage=new JLabel("");
		dobErrorMessage=new JLabel("");
		addressErrorMessage=new JLabel("");
		phoneErrorMessage=new JLabel("");


		namefield=new JTextField(30);
		emailfield=new JTextField(15);
		phonefield=new JTextField(15);
		dobfield=new JTextField(15);
		addressfield=new JTextArea(100,100);
		
		male= new JRadioButton("Male");
		female=new JRadioButton("Female");
		other=new JRadioButton("Other");

		bg=new ButtonGroup();
		bg.add(male);
		bg.add(female);
		bg.add(other);

		submit=new JButton("Submit");
		back=new JButton("Back");

		name.setBounds(10,10,50,20);
		namefield.setBounds(60,10,100,20);

		gender.setBounds(10,50,50,20);
		male.setBounds(60,50,70,20);
		female.setBounds(140,50,70,20);
		other.setBounds(220,50,70,20);

		dob.setBounds(10,90,80,20);
		dobfield.setBounds(100,90,100,20);

		email.setBounds(10,130,50,20);
		emailfield.setBounds(60,130,200,20);

		phone.setBounds(10,170,80,20);
		phonefield.setBounds(90,170,220,20);

		address.setBounds(10,210,80,20);
		addressfield.setBounds(90,210,200,100);

		back.setBounds(10,400,100,30);
		submit.setBounds(120,400,100,30);				

		contentPane.add(name);
		contentPane.add(namefield);

		contentPane.add(gender);
		contentPane.add(male);
		contentPane.add(female);
		contentPane.add(other);

		contentPane.add(dob);
		contentPane.add(email);
		contentPane.add(phone);
		contentPane.add(address);

		
		contentPane.add(emailfield);
		contentPane.add(phonefield);
		contentPane.add(dobfield);
		contentPane.add(addressfield);

		contentPane.add(submit);
		contentPane.add(back);

		back.addActionListener(this);
		submit.addActionListener(this);

	}

	public void actionPerformed(ActionEvent ae)
	{
		if(ae.getSource()==back){
			this.setVisible(false);
			mf.setVisible(true);
		}else if(ae.getSource()==submit){

			if(male.isSelected())
			{
				this.genderfield="male";
			}else if(female.isSelected()){
				this.genderfield="female";
			}else if(other.isSelected()){
				this.genderfield="other";
			}

			boolean isProperData=checkData();
			
			if(isProperData)
			{
				xmlCode=new StringBuffer("\t<contact>\n\t\t<name>"+ namefield.getText().replaceAll("( )+", " ") +"</name>");
				xmlCode.append("\n\t\t<phone_number>"+ phonefield.getText().replaceAll("( )+", " ") +"</phone_number>");
				xmlCode.append("\n\t\t<email>"+ emailfield.getText().replaceAll("( )+", " ") +"</email>");
				xmlCode.append("\n\t\t<dob>"+ dobfield.getText().replaceAll("( )+", " ") + "</dob>");
				xmlCode.append("\n\t\t<gender>"+ this.genderfield +"</gender>");
				xmlCode.append("\n\t\t<address>"+ addressfield.getText().replaceAll("\\s"," ").replaceAll("( )+", " ").trim() +"</address>\n\r");
				xmlCode.append("\t</contact>\r\n");

				System.out.println(xmlCode);

				this.processData(xmlCode);
				// mf.xmlCode=mf.getData();
				// mf.displayData();
				this.setVisible(false);
				new MainFrame();	
			}
			
		}

	}

	void processData(StringBuffer xmlCode)
	{
		int insertAtPos=0;
		String cpattern="</contact_list>";
		Pattern pattern=Pattern.compile(cpattern);
		Matcher matcher=pattern.matcher(mf.xmlCode);
		if(matcher.find()){
			// System.out.println("Occurence: "+matcher.start()+ "to "+matcher.end());
			insertAtPos=matcher.start();
		}
		mf.xmlCode.insert(insertAtPos,xmlCode);
		System.out.println(insertAtPos);
		// System.out.println(mf.xmlCode);

		try{
			File f=new File("contactList.xml");
			FileOutputStream fout=new FileOutputStream(f);

			PrintWriter pw=new PrintWriter(fout,true);
			BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
			pw.print("");
			pw.print(mf.xmlCode);
			pw.close();
			br.close();
			fout.close();
		}catch(IOException e){
			System.out.println("IO caught");
		}	
				
	}


	boolean checkData()
	{
		String nameErrmessage=va.validateName(namefield.getText());
		String phoneErrmessage=va.validatePhoneno(phonefield.getText());
		String emailErrmessage=va.validateEmail(emailfield.getText());
		String dobErrmessage=va.validateDob(dobfield.getText());
		String addressErrmessage=va.validateAddress(addressfield.getText());
		
		System.out.println(dobErrmessage);
		if(!nameErrmessage.equals(""))
		{				
			nameErrorMessage.setText(nameErrmessage);
			nameErrorMessage.setBounds(60,30,250,20);
			nameErrorMessage.setForeground(Color.red);
			contentPane.add(nameErrorMessage);
			namefield.setBorder(redline);
		}else{
			nameErrorMessage.setText("");
			nameErrorMessage.setBounds(60,30,250,20);
			contentPane.add(nameErrorMessage);
			namefield.setBorder(grayline);
		}

		if(!phoneErrmessage.equals(""))
		{
			phoneErrorMessage.setText(phoneErrmessage);
			phoneErrorMessage.setBounds(90,190,250,20);
			phoneErrorMessage.setForeground(Color.red);
			contentPane.add(phoneErrorMessage);
			phonefield.setBorder(redline);
		}else{
			phoneErrorMessage.setText("");
			phoneErrorMessage.setBounds(90,190,250,20);
			contentPane.add(phoneErrorMessage);
			phonefield.setBorder(grayline);
		}

		if(!emailErrmessage.equals(""))
		{	
			emailErrorMessage.setText(emailErrmessage);
			emailErrorMessage.setBounds(60,150,250,20);
			emailErrorMessage.setForeground(Color.red);
			contentPane.add(emailErrorMessage);
			emailfield.setBorder(redline);
		}else{
			emailErrorMessage.setText("");
			emailErrorMessage.setBounds(60,150,250,20);
			contentPane.add(emailErrorMessage);
			emailfield.setBorder(grayline);
		}

		if(!dobErrmessage.equals(""))
		{	
			dobErrorMessage.setText(dobErrmessage);
			dobErrorMessage.setBounds(100,110,350,20);
			dobErrorMessage.setForeground(Color.red);
			contentPane.add(dobErrorMessage);
			dobfield.setBorder(redline);
		}else{
			dobErrorMessage.setText("");
			dobErrorMessage.setBounds(100,110,250,20);
			contentPane.add(dobErrorMessage);
			dobfield.setBorder(grayline);
		}

		if(!addressErrmessage.equals(""))
		{	
			addressErrorMessage.setText(addressErrmessage);
			addressErrorMessage.setBounds(90,310,250,20);
			addressErrorMessage.setForeground(Color.red);
			contentPane.add(addressErrorMessage);
			addressfield.setBorder(redline);
		}else{
			addressErrorMessage.setText("");
			addressErrorMessage.setBounds(90,310,250,20);
			contentPane.add(addressErrorMessage);
			addressfield.setBorder(grayline);
		}

		
		if(this.genderfield.equals(""))
		{
			genderErrorMessage.setText("This field is required");
			genderErrorMessage.setBounds(70,70,250,20);
			genderErrorMessage.setForeground(Color.red);
			contentPane.add(genderErrorMessage);
		}else{
			genderErrorMessage.setText("");
			genderErrorMessage.setBounds(70,70,250,20);
			contentPane.add(genderErrorMessage);
		}

		if(!nameErrmessage.equals("") || !phoneErrmessage.equals("") || !emailErrmessage.equals("") || !dobErrmessage.equals("") || !addressErrmessage.equals(""))
		{
			repaint();
			return false;
		}

		return true;
	}

}

