import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;
import javax.swing.table.*;

class MainFrame extends JFrame implements ActionListener
{
	XMLParser xp;
	JScrollPane jsp;
	JPanel buttonsPanel;
	JTable table;
	JButton addContact,updateContact,deleteContact;
	StringBuffer xmlCode;
	Vector<String> names=new Vector<>();
	Vector<String> genders=new Vector<>();
	Vector<String> dobs=new Vector<>();
	Vector<String> emails=new Vector<>();
	Vector<String> phonenos=new Vector<>();
	Vector<String> addresses=new Vector<>();
	Vector<String> selectedValues=new Vector<>();
	

	MainFrame()
	{
		super("Contact Management");
		setSize(600,600);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		xp=new XMLParser();

		this.xmlCode=this.getData();
		this.displayData();

		Container contentPane=getContentPane();
		contentPane.setLayout(new BorderLayout());
		
		buttonsPanel=new JPanel();
		buttonsPanel.setSize(100,100);
		addContact=new JButton("Add Contact");
		updateContact=new JButton("Update Contact");
		deleteContact=new JButton("Delete Contact");
	
		buttonsPanel.add(addContact);
		buttonsPanel.add(updateContact);
		buttonsPanel.add(deleteContact);
		
		contentPane.add(jsp,BorderLayout.CENTER);
		contentPane.add(buttonsPanel,BorderLayout.SOUTH);
		
		addContact.addActionListener(this);
		updateContact.addActionListener(this);
		deleteContact.addActionListener(this);
		setVisible(true);

	}

	public void actionPerformed(ActionEvent ae)
	{
		if(ae.getSource()==addContact)
		{
			this.setVisible(false);
			AddContact ac=new AddContact(this);
			ac.displayForm();
		}else if(ae.getSource()==deleteContact){
			int totalColumns = table.getColumnCount();
			int row = table.getSelectedRow();
			selectedValues.removeAllElements();
			if(row>-1){
				for(int i=0;i<totalColumns;i++)
				{
					String value = table.getModel().getValueAt(row, i).toString();
					selectedValues.add(value);
					// System.out.println(value);	
				}

				StringBuffer contactToBeDeleted=new StringBuffer("\\t<contact>\\s*<name>"+ selectedValues.get(0) +"</name>");
				contactToBeDeleted.append("\\s*<phone_number>"+ selectedValues.get(4) +"</phone_number>");
				contactToBeDeleted.append("\\s*<email>"+ selectedValues.get(3) +"</email>");
				contactToBeDeleted.append("\\s*<dob>"+ selectedValues.get(2) + "</dob>");
				contactToBeDeleted.append("\\s*<gender>"+ selectedValues.get(1) +"</gender>");
				contactToBeDeleted.append("\\s*<address>"+ selectedValues.get(5) +"</address>\\s*");
				contactToBeDeleted.append("\\s*</contact>\\s*");

				System.out.println(contactToBeDeleted);
				String oldXml = this.xmlCode.toString();
				String newXml = oldXml.replaceAll(contactToBeDeleted.toString(), "");
				this.xmlCode.setLength(0); 
				this.xmlCode.append(newXml);
				System.out.println(xmlCode);

				try{
				File f=new File("contactList.xml");
				FileOutputStream fout=new FileOutputStream(f);

				PrintWriter pw=new PrintWriter(fout,true);
				BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
				pw.print("");
				pw.print(xmlCode);
				pw.close();
				br.close();
				fout.close();
				this.setVisible(false);
				new MainFrame();
			}catch(IOException e){
			System.out.println("IO caught");
			}
		}
		}else if(ae.getSource()==updateContact){
			int totalColumns = table.getColumnCount();
			int row = table.getSelectedRow();
			selectedValues.removeAllElements();
			if(row>-1){
				for(int i=0;i<totalColumns;i++)
				{
					String value = table.getModel().getValueAt(row, i).toString();
					selectedValues.add(value);
					// System.out.println(value);	
				}

				StringBuffer contactToBeUpdated=new StringBuffer("\\t<contact>\\s*<name>"+ selectedValues.get(0) +"</name>");
				contactToBeUpdated.append("\\s*<phone_number>"+ selectedValues.get(4) +"</phone_number>");
				contactToBeUpdated.append("\\s*<email>"+ selectedValues.get(3) +"</email>");
				contactToBeUpdated.append("\\s*<dob>"+ selectedValues.get(2) + "</dob>");
				contactToBeUpdated.append("\\s*<gender>"+ selectedValues.get(1) +"</gender>");
				contactToBeUpdated.append("\\s*<address>"+ selectedValues.get(5) +"</address>\\s*");
				contactToBeUpdated.append("\\s*</contact>\\s*");

				String oldXml = this.xmlCode.toString();
				UpdateContact uc=new UpdateContact(this,oldXml,contactToBeUpdated);
				uc.displayForm();
			}else{

			}
		}
	}

	public StringBuffer getData()
	{
		StringBuffer content=new StringBuffer();
		content.append("");
		try{
			File f=new File("contactList.xml");
			FileInputStream fis=new FileInputStream(f);

			int Character;
			while((Character=fis.read())!=-1)
			{
				// content+=(char)Character;
				content.append((char)Character);
			}
			
			// br.close();
			fis.close();
		}catch(FileNotFoundException e){
			System.out.println("File not found caught!");
		}catch(IOException e){
			System.out.println("IO caught");
		}
		return content;
	}

	void displayData(){
		names=xp.parseData(this.xmlCode,"name");
		genders=xp.parseData(this.xmlCode,"gender");
		dobs=xp.parseData(this.xmlCode,"dob");
		emails=xp.parseData(this.xmlCode,"email");
		phonenos=xp.parseData(this.xmlCode,"phone_number");
		addresses=xp.parseData(this.xmlCode,"address");

		// Enumeration vEnum=names.elements();
		// while(vEnum.hasMoreElements()){
		// 	System.out.println(vEnum.nextElement());
		// }

		Object[][] dataFromVectors=new Object[names.size()][6];
		for(int i=0;i<names.size();i++)
		{
			// System.out.println(names.get(i)+","+genders.get(i)+","+dobs.get(i)+","+emails.get(i)+","+phonenos.get(i)+","+addresses.get(i));
			dataFromVectors[i][0]=names.get(i);
			dataFromVectors[i][1]=genders.get(i);
			dataFromVectors[i][2]=dobs.get(i);
			dataFromVectors[i][3]=emails.get(i);
			dataFromVectors[i][4]=phonenos.get(i);
			dataFromVectors[i][5]=addresses.get(i);
		}

		final String [] colHeads ={"Name","Gender","Date Of Birth","Email","Phone-no","Address"};
		Object[][] data=dataFromVectors;

		// for(int i=0;i<names.size();i++)
		// {
		// 	for(int j=0;j<6;j++)
		// 		System.out.println(data[i][j]);
		// }
		table=new JTable(data,colHeads);
		table.setModel(new DefaultTableModel(data,colHeads));
		int v=ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED;
		int h=ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED;
		jsp=new JScrollPane(table,v,h);
	}

}

class ContactManagement{
	public static void main(String[] args) {
		new MainFrame();
	}
}