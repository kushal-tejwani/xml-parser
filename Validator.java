import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Validator
{
	String validateName(String name)
	{
		String nameRegex="[^a-zA-Z\\s]";
		Pattern namePattern=Pattern.compile(nameRegex);
		Matcher nameMatcher=namePattern.matcher(name);

		if(name.equals("")){
			return "This field is required";
		}else if(name.length()<2){
			return"Name must be atleast 2 characters";
		}else if(nameMatcher.find()){
			return "Name should contain only alphabets";
		}
		return "";
	}
	String validatePhoneno(String mobileNumber)
	{
		String mobileNumberRegex="^[7-9]\\d{9}$";
		Pattern mobileNumberPattern=Pattern.compile(mobileNumberRegex);
		Matcher mobileNumberMatcher=mobileNumberPattern.matcher(mobileNumber);

		if(mobileNumber.equals("")){
			return "This field is required";
		}
		else if(!mobileNumberMatcher.matches())
		{
			return "Phone no is invalid";
		}
		return "";
	}

	String validateEmail(String email)
	{
		String emailRegex="^[A-Za-z_]([\\.A-Za-z0-9\\+-_]+)*@([A-Za-z_]([A-Za-z0-9-])*)(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z0-9]{2,})$";
		Pattern emailPattern=Pattern.compile(emailRegex);
		Matcher emailMatcher=emailPattern.matcher(email);

		if(email.equals("")){
			return "This field is required";
		}
		else if(!emailMatcher.matches())
		{
			return "Email is invalid";
		}
		return "";
	}

	String validateDob(String dob)
	{
		String dobRegex="(0[1-9]|[12][0-9]|3[01])-(0[1-9]|1[1,2])-(19|20)\\d{2}";
		Pattern dobPattern=Pattern.compile(dobRegex);
		Matcher dobMatcher=dobPattern.matcher(dob);

		if(dob.equals("")){
			return "This field is required";
		}
		else if(!dobMatcher.matches())
		{
			return "Date is invalid please enter in dd-mm-yyyy format";
		}
		return "";
	}

	String validateAddress(String address)
	{
		if(address.equals("")){
			return "This field is required";
		}
		
		return "";
	}

}