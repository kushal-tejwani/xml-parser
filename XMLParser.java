import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Vector;


class XMLParser
{

	public Vector<String> parseData(StringBuffer xmlCode,String rootNode)
	{
		Vector<String>parsedData=new Vector<>();
		String groupRegex="(<"+rootNode+">)(.*?)(</"+rootNode+">)";
		Pattern groupPattern=Pattern.compile(groupRegex);
		Matcher groupMatcher=groupPattern.matcher(xmlCode);
		// System.out.println(groupMatcherReluctant.matches());

		while(groupMatcher.find())
		{
				// System.out.println("Internal Text: "+groupMatcher.group(2));
				parsedData.add(groupMatcher.group(2));
		}

		return parsedData;
	}

	public String parseSingleContact(StringBuffer xmlCode,String rootNode)
	{
		String parsedData="";
		String groupRegex="(<"+rootNode+">)(.*?)(</"+rootNode+">)";
		Pattern groupPattern=Pattern.compile(groupRegex);
		Matcher groupMatcher=groupPattern.matcher(xmlCode);
		// System.out.println(groupMatcherReluctant.matches());

		if(groupMatcher.find())
		{
				// System.out.println("Internal Text: "+groupMatcher.group(2));
				parsedData=groupMatcher.group(2);
		}
		return parsedData;
	}
}